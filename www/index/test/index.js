
var vows   = require( 'vows' ),
    assert = require( 'assert' );

var tvindex = require( '../tvindex' );

var FakeFs = require( './util/fakefs' ).FakeFs;

vows.describe( 'Navigating directories' ).addBatch( {

    'when the search directory is empty' : {
        topic: function() {
            tvindex.walk_dir(
                new FakeFs( { "home": {} } ),
                "/home",
                this.callback
            );
        },
        'no files are found' : function( err, topic ) {
            assert.isNull( err );
            assert.isEmpty( topic )
        }
    },

    'when the search directory is a missing root' : {
        topic: function() {
            tvindex.walk_dir(
                new FakeFs( { "home": {} } ),
                "/nonexistent",
                this.callback
            );
        },
        'it is an error' : function( err, topic ) {
            assert.isString( err )
        }
    },

    'when the search directory is a missing subdir' : {
        topic: function() {
            tvindex.walk_dir(
                new FakeFs( { "home": {} } ),
                "/home/nonexistent",
                this.callback
            );
        },
        'it is an error' : function( err, topic ) {
            assert.isString( err )
        }
    },

    'when there is a file in the search dir' : {
        topic: function() {
            tvindex.walk_dir(
                new FakeFs(
                    { "home": { "andy" : { "vids" : {
                        "foo.mp4": "",
                        "foo.rtvinfo": "title=Foo\ndescription=Foo desc\n"
                    } } } }
                ),
                "/home/andy/vids",
                this.callback
            );
        },
        'it is found' : function( err, topic ) {
            assert.isNull( err );
            assert.deepEqual( topic, [ "/home/andy/vids/foo.mp4" ] )
        }
    },

    'when there are multiple files in the search dir' : {
        topic: function() {
            tvindex.walk_dir(
                new FakeFs(
                    { "home": { "andy" : {
                        "foo.mp4": "",
                        "foo.rtvinfo": "title=Foo\ndescription=Foo desc\n",
                        "bar.mp4": "",
                        "bar.rtvinfo": "title=Bar\ndescription=Bar desc\n"
                    } } }
                ),
                "/home/andy",
                this.callback
            );
        },
        'they are found' : function( err, topic ) {
            assert.isNull( err );
            assert.deepEqual(
                topic,
                [
                    "/home/andy/bar.mp4",
                    "/home/andy/foo.mp4"
                ]
            )
        }
    },

    'when there are files in a subdir' : {
        topic: function() {
            tvindex.walk_dir(
                new FakeFs(
                    { "home": { "andy" : { "vids" : {
                        "foo.mp4": "",
                        "foo.rtvinfo": "title=Foo\ndescription=Foo desc\n",
                        "bar.mp4": "",
                        "bar.rtvinfo": "title=Bar\ndescription=Bar desc\n"
                    } } } }
                ),
                "/home/andy",
                this.callback
            );
        },
        'they are found' : function( err, topic ) {
            assert.isNull( err );
            assert.deepEqual(
                topic,
                [
                    "/home/andy/vids/bar.mp4",
                    "/home/andy/vids/foo.mp4"
                ]
            );
        }
    }
} ).export( module );



