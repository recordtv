
exports.FakeFs = function( obj )
{
    this._find_path = function( path, callback )
    {
        if ( path[0] !== "/" )
        {
            callback(
                "Path '" + "' does not start with /," +
                "but it must (for now)."
            );
            return;
        }

        var dirs = path.split( "/" );
        function impl( n, subobj )
        {
            var dir = dirs[n];

            if ( !( dir in subobj ) )
            {
                callback( "Path '" + path + "' does not exist." );
                return;
            }

            var found = subobj[dir];

            if ( n === dirs.length - 1 )
            {
                callback( null, found );
            }
            else
            {
                impl( n + 1, found );
            }
        }

        impl( 1, obj );
    };


    this.readdir = function( path, callback )
    {
        this._find_path( path, function( err, found )
        {
            if ( err )
            {
                callback( err );
            }
            else if ( typeof found === "string" )
            {
                callback( "'" + path + "' is a file, not a directory." );
            }
            else
            {
                var ret = [];
                for ( var name in found )
                {
                    ret.push( name );
                }
                ret.sort();
                callback( null, ret );
            }
        } );
    };

    this.stat = function( path, callback )
    {
        this._find_path( path, function( err, found )
        {
            if ( err )
            {
                callback( err );
            }
            else
            {
                callback(
                    null,
                    {
                        isDirectory: function()
                        {
                            return ( typeof found !== "string" );
                        }
                    }
                );
            }
        } );
    };
}

