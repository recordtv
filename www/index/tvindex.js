
exports.walk_dir = function( fs, dir, callback )
{
    var ret_files = [];
    var ret_err = null;

    function implDir( queue, item )
    {
        fs.readdir( item, function( err, subitems )
        {
            if ( err )
            {
                callback( err );
                ret_err = true;
            }
            else
            {
                for ( var i = 0; i < subitems.length; ++i  )
                {
                    queue.push( item + "/" + subitems[i] );
                }
                impl( queue );
            }
        } );
    }

    function implFile( queue, item )
    {
        if ( item.match( /.*\.mp4/ ) )
        {
            ret_files.push( item );
        }
        impl( queue );
    }

    function implItem( queue, item )
    {
        fs.stat( item, function( err, stats )
        {
            if ( err )
            {
                ret_err = true;
                callback( err );
            }
            else if ( stats.isDirectory() )
            {
                implDir( queue, item );
            }
            else
            {
                implFile( queue, item );
            }
        } );
    }

    function impl( queue )
    {
        if ( ret_err )
        {
            return;
        }
        else if ( queue.length === 0 )
        {
            ret_files.sort();
            callback( null, ret_files );
        }
        else
        {
            var item = queue.pop();
            implItem( queue, item );
        }
    }

    impl( [ dir ] );

}

