## Process this file with automake to produce Makefile.in

APP_NAME=recordtv
TMP_DIR=/tmp/$(APP_NAME)
VERSION=0.0.1

pkg-src:
	mkdir -p pkg
	- rm -f pkg/$(APP_NAME)-*.tar.bz2
	- rm -r $(TMP_DIR)
	mkdir $(TMP_DIR)
	cg-export $(TMP_DIR)/$(APP_NAME)-$(VERSION)/
	tar --exclude uploadweb.sh --directory $(TMP_DIR)/ -cjf pkg/$(APP_NAME)-$(VERSION).tar.bz2 \
		$(APP_NAME)-$(VERSION)
	rm -r $(TMP_DIR)

# Push the current source code to my git repository
git-upload:
	- rm -r $(TMP_DIR)
	git-repack -d
	git clone --bare -l . $(TMP_DIR)/$(APP_NAME).git
	rsync -r --delete $(TMP_DIR)/$(APP_NAME).git/ \
		artific2@artificialworlds.net:public_html/$(APP_NAME)/git/$(APP_NAME).git/
	rm -rf $(TMP_DIR);


