#!/usr/bin/python

import datetime, time, re, os
import rtv_utils

DATETIME_FORMAT = "%Y-%m-%d-%H:%M"
datetime_re = re.compile( "\\d{4}-\\d{2}-\\d{2}-\\d{2}:\\d{2}" )

class AbstractPropertiesFile( object ):

	def load( self, filename ):
		f = file( filename, "r" )

		for ln in f:
			ln_split = ln[:-1].split( "=", 1 )
			len_ln_split = len( ln_split )

			if len_ln_split > 0:
				if len( ln_split ) == 2:
					( key, value ) = ln_split
				else:
					key = ln_split[0]
					value = ""
				key = key.strip()
				value = value.strip()
				if datetime_re.match( value ):
					self.set_value( key, rtv_utils.parse_datetime( value,
						DATETIME_FORMAT ) )
				elif len( value ) == 0:
					self.set_value( key, value )
				elif value[0] == "|":
					self.set_value( key, value[1:].split( "|" ) )
				elif value[:2] == "_|" or value == "_False" or value == "_True":
					self.set_value( key, value[1:] )
				elif value == "False":
					self.set_value( key, False )
				elif value == "True":
					self.set_value( key, True )
				else:
					try:
						self.set_value( key, int(value) )
					except ValueError:
						try:
							self.set_value( key, float(value) )
						except ValueError:
							self.set_value( key, value )

		f.close()

	def save( self, filename ):

		f = file( filename, "w" )
		for k in self.get_keys():
			if k[0] != "_":
				k = self._prepare_key( k )
				v = self._prepare_value( self.get_value( k ) )

				if v != None:
					f.write( rtv_utils.encode_text( k ) )
					f.write( "=" )
					f.write( rtv_utils.encode_text( v ) )
					f.write( "\n" )

		f.close()

	def _prepare_key( self, key ):
		return key.replace( "=", "_" )

	def _prepare_value( self, value ):
		tp = type( value )

		if value == None or value == "":
			return None
		elif tp == int or tp == float:
			return str( value )
		elif tp == str or tp == unicode:
			if len(value) > 0 and value[0] == "|":
				value = "_|" + value[1:]
			elif value == "False":
				value = "_False"
			elif value == "True":
				value = "_True"
			return value
		elif tp == tuple or tp == list:
			if len( value ) == 0:
				return None
			else:
				ret = ""
				for v in value:
					vtp = type( v )
					if vtp == int or vtp == float:
						ret += "|"
						ret += str( v )
					elif vtp == str or vtp == unicode:
						ret += "|"
						ret += v.replace( "|", "_" )
					else:
						ret += "|<Unknown list type %s>>" % vtp
				return ret
		elif tp == datetime.datetime:
			return value.strftime( DATETIME_FORMAT )
		elif tp == bool:
			return str( value )
		else:
			return "<<Unknown type %s>>" % tp

	def set_value( self, key, value ):
		raise Exception( "Method not implemented." )

	def get_value( self, key ):
		raise Exception( "Method not implemented." )

	def get_keys( self ):
		raise Exception( "Method not implemented." )


