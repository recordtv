#!/usr/bin/python

import time, datetime, os, re
import rtv_favourite, rtv_utils, rtv_programmeinfo
import rtv_propertiesfile, rtv_selection

# TODO: delete old files:
#          - scheduled recordings
#          - logs
#          - selections

MESSAGE_TIME_FORMAT = "%H:%M on %a"

at_output_re = re.compile( "job (\d+) at .*\n" )

def priority_time_compare( pi1, pi2 ):
	pi1Pri = pi1.get_priority()
	pi2Pri = pi2.get_priority()

	if pi1Pri > pi2Pri:
		return -1
	elif pi1Pri < pi2Pri:
		return 1
	elif pi1.startTime < pi2.startTime:
		return -1
	elif pi1.startTime > pi2.startTime:
		return 1

	return 0

class Schedule:

	def __init__( self, config ):
		self.config = config

	def sub_title_matters( self, proginfo ):
		return ( proginfo.sub_title is not None
			and proginfo.unique_subtitles == True )

	def add_to_old_progs_map( self, dr, fn, ret, delete_unused ):
		if not fn.endswith( ".rtvinfo" ):
			return

		full_path = os.path.join( dr, fn )
		proginfo = rtv_programmeinfo.ProgrammeInfo()
		proginfo.load( full_path )

		if not self.sub_title_matters( proginfo ):
			if delete_unused:
				os.remove( full_path )
			return

		if proginfo.title not in ret:
			ret[proginfo.title] = {}

		if proginfo.sub_title not in ret[proginfo.title]:
			ret[proginfo.title][proginfo.sub_title] = 1

	def find_old_programmes_map( self, old_dir, converted_dir ):
		ret = {}

		dr_list = os.listdir( old_dir )
		for fn in dr_list:
			self.add_to_old_progs_map( old_dir, fn, ret, True )

		for (dirpath, dirnames, filenames) in os.walk( converted_dir ):
			for fn in filenames:
				self.add_to_old_progs_map( dirpath, fn, ret, False )

		return ret

	def get_at_job( self, at_output ):
		for ln in at_output:
			m = at_output_re.match( ln )
			if m:
				return m.group( 1 )
		print ( "** Unable to understand at command output '%s' - "
			+ "can't create a scheduled_events entry **" ) % at_output
		return None

	def print_already_recorded( self, prog ):
		print self.pretty_title( prog )

	def print_recording_today( self, prog ):
		print ( "%s (recording another showing today)"
			% self.pretty_title( prog ) )

	def remove_already_recorded( self, scheduler ):
		print
		print "Skipped (already recorded):"

		old_dir = os.path.join( self.config.recorded_progs_dir, "old" )
		converted_dir = self.config.converted_progs_dir
		old_progs_map = scheduler.find_old_programmes_map( old_dir,
			converted_dir )

		new_queue = []
		new_queue_sub_titles_map = {}

		for prog in self.record_queue:
			if ( prog.title in old_progs_map and
					prog.sub_title in old_progs_map[prog.title] ):
				self.print_already_recorded( prog )
			elif ( prog.title in new_queue_sub_titles_map and
					prog.sub_title is not None and
					prog.sub_title in new_queue_sub_titles_map[prog.title] ):
				self.print_recording_today( prog )
			else:
				new_queue.append( prog )
				if self.sub_title_matters( prog ):
					new_queue_sub_titles_map[prog.title] = prog.sub_title

		self.record_queue = new_queue


	def print_clash_priority_error( self, losePi, keepPi ):
		print ( ("%s (priority %d) < (priority %d) %s")
				% ( self.pretty_title( losePi ), losePi.get_priority(),
					keepPi.get_priority(), self.pretty_title( keepPi ) ) )

	def print_clash_time_error( self, losePi, keepPi ):
		print ( ("%s (%s) is later than (%s) %s")
				% ( self.pretty_title( losePi ),
					self.pretty_time( losePi ),
					self.pretty_time( keepPi ),
					self.pretty_title( keepPi ) ) )

	def print_clash_same_time( self, losePi, keepPi ):
		print ( ("%s lost randomly to %s")
				% ( self.pretty_title( losePi ),
					self.pretty_title( keepPi ) ) )

	def remove_clashes( self ):
		print
		print "Skipped (clashes):"

		self.record_queue.sort( priority_time_compare )
		new_queue = []

		for pi1Num in range( len( self.record_queue ) ):
			pi1 = self.record_queue[pi1Num]

			clashed_with = None

			for pi2 in new_queue:

				if pi1.clashes_with( pi2 ):
					clashed_with = pi2
					break

			if not clashed_with:
				new_queue.append( pi1 )
			else:
				if pi1.get_priority() < clashed_with.get_priority():
					self.print_clash_priority_error( pi1, clashed_with )
				elif pi1.startTime > clashed_with.startTime:
					self.print_clash_time_error( pi1, clashed_with )
				else:
					self.print_clash_same_time( pi1, clashed_with )

		self.record_queue = new_queue

	def pretty_title( self, programmeInfo ):
		if programmeInfo.sub_title is None:
			return programmeInfo.title
		else:
			return "%s: %s" % ( programmeInfo.title, programmeInfo.sub_title )

	def pretty_time( self, programmeInfo ):
		return programmeInfo.startTime.strftime( MESSAGE_TIME_FORMAT )

	def schedule_recordings( self, queue ):

		if len( queue ) == 0:
			print
			print "No programmes found to record."
			return

		rtv_utils.ensure_dir_exists( self.config.recorded_progs_dir )
		rtv_utils.ensure_dir_exists( self.config.scheduled_events_dir )
		rtv_utils.ensure_dir_exists( self.config.recording_log_dir )

		print
		print "Recording:"

		for programmeInfo in queue:

			print ( "%s (%s)"
				% ( self.pretty_title( programmeInfo ),
					self.pretty_time( programmeInfo ) ) )

			filename = rtv_utils.prepare_filename( programmeInfo.title )
			filename += programmeInfo.startTime.strftime( "-%Y-%m-%d_%H_%M" )

			length_timedelta = programmeInfo.endTime - programmeInfo.startTime
			length_in_seconds = ( ( length_timedelta.days
				* rtv_utils.SECS_IN_DAY ) + length_timedelta.seconds )
			length_in_seconds += 60 * self.config.extra_recording_time_mins

			sched_filename = os.path.join( self.config.scheduled_events_dir,
				filename + ".rtvinfo" )

			outfilename = os.path.join( self.config.recorded_progs_dir,
				filename )
			cmds_array = ( "at",
				programmeInfo.startTime.strftime( "%H:%M %d.%m.%Y" ) )

			if programmeInfo.filetype is not None:
				filetype = programmeInfo.filetype
			else:
				filetype = ""

			at_output = rtv_utils.run_command_feed_input( cmds_array,
				self.config.record_start_command % (
					self.channel_xmltv2tzap.get_value( programmeInfo.channel ),
					outfilename, length_in_seconds, sched_filename, filetype,
					os.path.join( self.config.recording_log_dir,
						filename + ".log" ) ) )
			at_job_start = self.get_at_job( at_output )

			if at_job_start != None:
				programmeInfo.atJob = at_job_start
				programmeInfo.save( sched_filename )

	def sax_callback( self, pi ):
		for fav in self.favs_and_sels:
			if fav.matches( pi ):
				dtnow = datetime.datetime.today()
				if( pi.startTime > dtnow
						and (pi.startTime - dtnow).days
							< self.config.options.days ):

					if fav.deleteAfterDays:
						pi.deleteTime = pi.endTime + datetime.timedelta(
							float( fav.deleteAfterDays ), 0 )

					pi.channel_pretty = self.channel_xmltv2tzap.get_value(
						pi.channel )
					if not pi.channel_pretty:
						print (
							"** Pretty channel name not found for channel %s **"
							% pi.channel )

					pi.priority         = fav.priority
					pi.destination      = fav.destination
					pi.unique_subtitles = fav.unique_subtitles
					pi.filetype         = fav.filetype

					if fav.real_title:
						pi.title = fav.real_title

					self.record_queue.append( pi )
				break

	def remove_scheduled_events( self ):

		rtv_utils.ensure_dir_exists( self.config.scheduled_events_dir )

		for fn in os.listdir( self.config.scheduled_events_dir ):
			full_fn = os.path.join( self.config.scheduled_events_dir, fn )

			pi = rtv_programmeinfo.ProgrammeInfo()
			pi.load( full_fn )

			done_atrm = False
			try:
				at_job_start = int( pi.atJob )
				rtv_utils.run_command( ( "atrm", str( at_job_start ) ) )
				done_atrm = True
			except ValueError:
				pass

			if done_atrm:
				os.unlink( full_fn )

	def delete_pending_recordings( self ):
		rtv_utils.ensure_dir_exists( self.config.recorded_progs_dir )

		dttoday = datetime.datetime.today()

		for fn in os.listdir( self.config.recorded_progs_dir ):
			if fn[-4:] in ( ".flv", ".avi" ):

				infofn = os.path.join( self.config.recorded_progs_dir,
					fn[:-4] + ".rtvinfo" )

				if os.path.isfile( infofn ):

					pi = rtv_programmeinfo.ProgrammeInfo()
					pi.load( infofn )

					if pi.deleteTime and pi.deleteTime < dttoday:
						full_fn = os.path.join(
							self.config.recorded_progs_dir, fn )

						print "Deleting file '%s'" % full_fn

						os.unlink( full_fn )
						os.unlink( infofn )



	def schedule( self, xmltv_parser = rtv_utils, fav_reader = rtv_selection,
			scheduler = None ):

		if scheduler == None:
			scheduler = self

		# TODO: if we are generating a TV guide as well as scheduling,
		#       we should share the same XML parser.

		self.delete_pending_recordings()

		self.favs_and_sels = fav_reader.read_favs_and_selections(
			self.config, record_only = True )

		self.channel_xmltv2tzap = rtv_propertiesfile.PropertiesFile()
		self.channel_xmltv2tzap.load( self.config.channel_xmltv2tzap_file )

		scheduler.remove_scheduled_events()

		self.record_queue = []
		xmltv_parser.parse_xmltv_files( self.config, self.sax_callback )
		self.remove_already_recorded( scheduler )
		self.remove_clashes()
		scheduler.schedule_recordings( self.record_queue )
		self.record_queue = []


def schedule( config ):
	sch = Schedule( config )
	sch.schedule()





# === Test code ===

def format_title_subtitle( pi ):
	ret = pi.title
	if pi.sub_title:
		ret += " : "
		ret += pi.sub_title
	return ret

def format_pi_list( qu ):
	ret = "[ "
	for pi in qu[:-1]:
		ret += format_title_subtitle( pi )
		ret += ", "
	if len( qu ) > 0:
		ret += format_title_subtitle( qu[-1] )
	ret += " ]"

	return ret

class FakeScheduler( object ):

	def __init__( self, schedule ):

		self.schedule = schedule
		self.remove_scheduled_events_called = False

		dtnow = datetime.datetime.today()

		favHeroes = rtv_favourite.Favourite()
		favHeroes.title_re = "Heroes"

		favNewsnight = rtv_favourite.Favourite()
		favNewsnight.title_re = "Newsnight.*"
		favNewsnight.priority = -50

		favLost = rtv_favourite.Favourite()
		favLost.title_re = "Lost"
		favLost.priority = -50

		favPocoyo = rtv_favourite.Favourite()
		favPocoyo.title_re = "Pocoyo"

		self.test_favs = [ favHeroes, favNewsnight, favLost, favPocoyo ]

		self.piHeroes = rtv_programmeinfo.ProgrammeInfo()
		self.piHeroes.title = "Heroes"
		self.piHeroes.startTime = dtnow + datetime.timedelta( hours = 1 )
		self.piHeroes.endTime = dtnow + datetime.timedelta( hours = 2 )
		self.piHeroes.channel = "south-east.bbc2.bbc.co.uk"

		self.piNewsnight = rtv_programmeinfo.ProgrammeInfo()
		self.piNewsnight.title = "Newsnight"
		self.piNewsnight.startTime = dtnow + datetime.timedelta( hours = 1,
			minutes = 30 )
		self.piNewsnight.endTime = dtnow + datetime.timedelta( hours = 2 )
		self.piNewsnight.channel = "south-east.bbc1.bbc.co.uk"

		self.piNR = rtv_programmeinfo.ProgrammeInfo()
		self.piNR.title = "Newsnight Review"
		self.piNR.startTime = dtnow + datetime.timedelta( hours = 2 )
		self.piNR.endTime = dtnow + datetime.timedelta( hours = 2,
			minutes = 30 )
		self.piNR.channel = "south-east.bbc2.bbc.co.uk"

		self.piLost = rtv_programmeinfo.ProgrammeInfo()
		self.piLost.title = "Lost"
		self.piLost.startTime = dtnow + datetime.timedelta( hours = 1,
			minutes = 25 )
		self.piLost.endTime = dtnow + datetime.timedelta( hours = 2,
			minutes = 30 )
		self.piLost.channel = "channel4.com"

		self.piTurnip = rtv_programmeinfo.ProgrammeInfo()
		self.piTurnip.title = "Newsnight Turnip"
		self.piTurnip.startTime = dtnow + datetime.timedelta( hours = 1,
			minutes = 30 )
		self.piTurnip.endTime = dtnow + datetime.timedelta( hours = 2,
			minutes = 30 )
		self.piTurnip.channel = "channel5.co.uk"

		self.piPocoyo1 = rtv_programmeinfo.ProgrammeInfo()
		self.piPocoyo1.title = "Pocoyo"
		self.piPocoyo1.sub_title = "Subtitle already seen"
		self.piPocoyo1.startTime = dtnow + datetime.timedelta( hours = 1 )
		self.piPocoyo1.endTime = dtnow + datetime.timedelta( hours = 2 )
		self.piPocoyo1.channel = "south-east.bbc2.bbc.co.uk"

		self.piPocoyo2 = rtv_programmeinfo.ProgrammeInfo()
		self.piPocoyo2.title = "Pocoyo"
		self.piPocoyo2.sub_title = "Subtitle not seen"
		self.piPocoyo2.startTime = dtnow + datetime.timedelta( hours = 2 )
		self.piPocoyo2.endTime = dtnow + datetime.timedelta( hours = 3 )
		self.piPocoyo2.channel = "south-east.bbc2.bbc.co.uk"

		self.which_test = None

	def find_old_programmes_map( self, old_dir, converted_dir ):
		return { self.piPocoyo1.title : { self.piPocoyo1.sub_title : 1 } }

	def parse_xmltv_files( self, config, callback ):
		if self.which_test == "no_clash1":
			callback( self.piNewsnight )
			callback( self.piNR )
		elif self.which_test == "priority_clash1":
			callback( self.piHeroes )
			callback( self.piNewsnight )
		elif self.which_test == "time_clash1":
			callback( self.piNewsnight )
			callback( self.piLost )
		elif self.which_test == "same_clash1":
			callback( self.piNewsnight )
			callback( self.piTurnip )
		elif self.which_test == "norerecord":
			callback( self.piPocoyo1 )
			callback( self.piPocoyo2 )

	def remove_scheduled_events( self ):
		if self.remove_scheduled_events_called:
			raise Exception( "remove_scheduled_events called twice." )
		self.remove_scheduled_events_called = True

	def read_favs_and_selections( self, config, record_only ):
		return self.test_favs

	def schedule_recordings( self, queue ):
		self.queue = queue

	def test( self ):
		self.which_test = "priority_clash1"
		self.schedule.schedule( self, self, self )
		if not self.remove_scheduled_events_called:
			raise Exception( "remove_scheduled_events never called" )
		if self.queue != [self.piHeroes]:
			raise Exception( "queue should look like %s, but it looks like %s"
				% ( [self.piHeroes], self.queue ) )


		self.which_test = "no_clash1"
		self.remove_scheduled_events_called = False
		self.schedule.schedule( self, self, self )
		if not self.remove_scheduled_events_called:
			raise Exception( "remove_scheduled_events never called" )
		if self.queue != [self.piNewsnight, self.piNR]:
			raise Exception( "queue should look like %s, but it looks like %s"
				% ( [self.piNewsnight, self.piNR], self.queue ) )

		self.which_test = "time_clash1"
		self.remove_scheduled_events_called = False
		self.schedule.schedule( self, self, self )
		if not self.remove_scheduled_events_called:
			raise Exception( "remove_scheduled_events never called" )
		if self.queue != [self.piLost]:
			raise Exception( "queue should look like %s, but it looks like %s"
				% ( [self.piLost], self.queue ) )

		self.which_test = "same_clash1"
		self.remove_scheduled_events_called = False
		self.schedule.schedule( self, self, self )
		if not self.remove_scheduled_events_called:
			raise Exception( "remove_scheduled_events never called" )
		if self.queue != [self.piNewsnight] and self.queue != [self.piTurnip]:
			raise Exception( ("queue should look like %s or %s, but it"
					+ " looks like %s" )
				% ( format_pi_list( [self.piNewsnight] ),
					format_pi_list( [self.piTurnip] ),
					format_pi_list( self.queue ) ) )

	def test_norerecord( self ):
		self.which_test = "norerecord"
		self.remove_scheduled_events_called = False
		self.schedule.schedule( self, self, self )

		if not self.remove_scheduled_events_called:
			raise Exception( "remove_scheduled_events never called" )
		if self.queue != [self.piPocoyo2]:
			raise Exception( ("queue should look like %s, but it"
					+ " looks like %s" )
				% ( format_pi_list( [self.piPocoyo2] ),
					format_pi_list( self.queue ) ) )



class FakeScheduler_SameTitleSameDay( FakeScheduler ):

	def __init__( self, schedule ):
		FakeScheduler.__init__( self, schedule )

	def find_old_programmes_map( self, old_dir, converted_dir ):
		return {}

	def parse_xmltv_files( self, config, callback ):
		dtnow = datetime.datetime.today()

		# 2 with no subtitle
		self.piPocoyo1 = rtv_programmeinfo.ProgrammeInfo()
		self.piPocoyo1.title = "Pocoyo"
		self.piPocoyo1.startTime = dtnow + datetime.timedelta( hours = 1 )
		self.piPocoyo1.endTime = dtnow + datetime.timedelta( hours = 2 )
		self.piPocoyo1.channel = "south-east.bbc2.bbc.co.uk"

		self.piPocoyo2 = rtv_programmeinfo.ProgrammeInfo()
		self.piPocoyo2.title = "Pocoyo"
		self.piPocoyo2.startTime = dtnow + datetime.timedelta( hours = 2 )
		self.piPocoyo2.endTime = dtnow + datetime.timedelta( hours = 3 )
		self.piPocoyo2.channel = "south-east.bbc2.bbc.co.uk"

		# 2 with identical subtitle
		self.piPocoyo3 = rtv_programmeinfo.ProgrammeInfo()
		self.piPocoyo3.title = "Pocoyo"
		self.piPocoyo3.sub_title = "Subtitle we will see twice"
		self.piPocoyo3.startTime = dtnow + datetime.timedelta( hours = 3 )
		self.piPocoyo3.endTime = dtnow + datetime.timedelta( hours = 4 )
		self.piPocoyo3.channel = "south-east.bbc2.bbc.co.uk"

		self.piPocoyo4 = rtv_programmeinfo.ProgrammeInfo()
		self.piPocoyo4.title = "Pocoyo"
		self.piPocoyo4.sub_title = "Subtitle we will see twice"
		self.piPocoyo4.startTime = dtnow + datetime.timedelta( hours = 4 )
		self.piPocoyo4.endTime = dtnow + datetime.timedelta( hours = 5 )
		self.piPocoyo4.channel = "south-east.bbc2.bbc.co.uk"

		callback( self.piPocoyo1 )
		callback( self.piPocoyo2 )
		callback( self.piPocoyo3 )
		callback( self.piPocoyo4 )

	def read_favs_and_selections( self, config, record_only ):
		favPocoyo = rtv_favourite.Favourite()
		favPocoyo.title_re = "Pocoyo"
		return [ favPocoyo ]

	def test( self ):
		self.schedule.schedule( self, self, self )

		if not self.remove_scheduled_events_called:
			raise Exception( "remove_scheduled_events never called" )

		expected_queue = [ self.piPocoyo1, self.piPocoyo2, self.piPocoyo3 ]

		if self.queue != expected_queue:
			raise Exception( ("queue should look like %s, but it"
					+ " looks like %s" )
				% ( format_pi_list( [self.piPocoyo3] ),
					format_pi_list( self.queue ) ) )


def test( config ):
	p = FakeScheduler( Schedule( config ) )

	p.test()
	p.test_norerecord()

	FakeScheduler_SameTitleSameDay( Schedule( config ) ).test()



