#!/usr/bin/python

import re
import rtv_propertiesfile, rtv_programmeinfo

class Favourite(rtv_propertiesfile.PropertiesFile):

	def __init__( self ):
		self.title_re = None
		self.sub_title_re = None
		self.channel = None
		self.not_channel = None
		self.deleteAfterDays = None
		self.priority = None
		self.record = "yes"
		self.destination = None
		self.real_title = None
		self.unique_subtitles = True
		self.filetype = None

		self._title_re = None
		self._sub_title_re = None


	def matches( self, pi ):

		if self.title_re is None:
			return False	# This favourite is empty

		if self._title_re is None:
			self._title_re = re.compile( self.title_re + "$" )

		if not self._title_re.match( pi.title ):
			return False

		if self.not_channel is not None and self.not_channel == pi.channel:
			return False

		if self.channel is not None and self.channel != pi.channel:
			return False

		if self.sub_title_re is not None and pi.sub_title is not None:
			if self._sub_title_re is None:
				self._sub_title_re = re.compile( self.sub_title_re + "$" )
			if not self._sub_title_re.match( pi.sub_title ):
				return False

		return True
		# TODO: other things to match on e.g. time, categories

class FakeProg( object ):
	def __init__( self, title, channel, sub_title = None ):
		self.title = title
		self.channel = channel
		self.sub_title = sub_title

def test_Empty_favourite_never_matches():
	fav = Favourite()
	assert( not fav.matches( FakeProg( "t", "c" ) ) )

def test_Wrong_title_doesnt_match():
	fav = Favourite()
	fav.title_re = "other"
	assert( not fav.matches( FakeProg( "this", "c" ) ) )

def test_Right_title_matches():
	fav = Favourite()
	fav.title_re = "this"
	assert( fav.matches( FakeProg( "this", "c" ) ) )

def test_Wrong_subtitle_doesnt_match():
	fav = Favourite()
	fav.title_re = "this"
	fav.sub_title_re = "other"
	assert( not fav.matches( FakeProg( "this", "c", "this_sub" ) ) )

def test_Right_subtitle_matches():
	fav = Favourite()
	fav.title_re = "this"
	fav.sub_title_re = "this_sub"
	assert( fav.matches( FakeProg( "this", "c", "this_sub" ) ) )

def test_Wrong_channel_doesnt_match():
	fav = Favourite()
	fav.title_re = "this"
	fav.channel = "oc"
	assert( not fav.matches( FakeProg( "this", "c" ) ) )

def test_Right_channel_does_match():
	fav = Favourite()
	fav.title_re = "this"
	fav.channel = "c"
	assert( fav.matches( FakeProg( "this", "c" ) ) )

def test_Not_channel_excludes():
	fav = Favourite()
	fav.title_re = "this"
	fav.not_channel = "Dave"
	assert( not fav.matches( FakeProg( "this", "Dave" ) ) )

def test_Unmatching_not_channel_doesnt_exclude():
	fav = Favourite()
	fav.title_re = "this"
	fav.not_channel = "Dave"
	assert( fav.matches( FakeProg( "this", "BBC One" ) ) )

def test( config ):
	test_Empty_favourite_never_matches()
	test_Wrong_title_doesnt_match()
	test_Right_title_matches()
	test_Wrong_subtitle_doesnt_match()
	test_Right_subtitle_matches()
	test_Wrong_channel_doesnt_match()
	test_Right_channel_does_match()
	test_Not_channel_excludes()
	test_Unmatching_not_channel_doesnt_exclude()

