#!/usr/bin/python

from rtv_abstractpropertiesfile import AbstractPropertiesFile

class OrderedPropertiesFile( AbstractPropertiesFile ):

	def __init__( self ):
		self.counter = 0

	def set_value( self, key, value ):
		if value not in self.__dict__:
			self.__dict__[key] = ( self.counter, value )
			self.counter += 1

	def get_value( self, key ):
		return self.__dict__[key][1]

	def has_key( self, key ):
		return key in self.__dict__

	def get_position( self, key ):
		return self.__dict__[key][0]

	def get_keys( self ):
		return self.__dict__.keys()

