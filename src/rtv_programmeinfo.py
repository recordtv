#!/usr/bin/python

import rtv_propertiesfile

class ProgrammeInfo( rtv_propertiesfile.PropertiesFile ):

	def __init__( self ):
		self.title = None
		self.sub_title = None
		self.description = None
		self.channel = None
		self.channel_pretty = None
		self.startTime = None
		self.endTime = None
		self.categories = []
		self.atJob = None
		self.deleteTime = None
		self.priority = None
		self.destination = None
		self.unique_subtitles = True


	def clashes_with( self, otherPi ):

		return (  ( otherPi.startTime >= self.startTime and
					otherPi.startTime < self.endTime )
				or
				  ( self.startTime >= otherPi.startTime and
					self.startTime < otherPi.endTime ) )

	def get_priority( self ):
		if self.priority == None:
			return 0
		else:
			return int( self.priority )
