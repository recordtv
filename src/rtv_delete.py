#!/usr/bin/python

import os
import shutil
import rtv_utils

def _delete_file( base_dir, filename ):
	fullpath = os.path.join( base_dir, filename )
	print fullpath
	os.remove( fullpath )

	mp3path = fullpath + ".mp3"
	if os.path.isfile( mp3path ):
		print mp3path
		os.remove( mp3path )

def collect_walk_no_slashes( top_dir ):
	len_top_dir = len( top_dir ) + 1
	ret = {}
	for ( dirpath, dirnames, filenames ) in os.walk( top_dir ):
		if os.path.basename( dirpath ) == "deleted":
			continue
		for filename in filenames:
			filepath = os.path.join( dirpath[len_top_dir:], filename )
			filepath_noslashes = filepath.replace( "/", "_" )
			ret[filepath_noslashes] = filepath
	return ret

def delete( config ):

	print
	print "Deleting:"

	deleted_dir = os.path.join( config.converted_progs_dir, "deleted" )

	rtv_utils.ensure_dir_exists( deleted_dir )

	noslash_to_real = collect_walk_no_slashes( config.converted_progs_dir )
	noslash_filenames = noslash_to_real.keys()

	old_dir = os.path.join( config.recorded_progs_dir, "old" )

	for fn in os.listdir( deleted_dir ):
		if fn in noslash_filenames:
			_delete_file( config.converted_progs_dir, noslash_to_real[fn] )
		else:
			print "Filename to delete '%s' not found!" % fn

		rtvinfo_fn = fn[ : fn.rfind( "." ) ] + ".rtvinfo"
		if rtvinfo_fn in noslash_filenames:

			# Move the old .rtvinfo file into the old directory
			# so we can avoid re-recording things that we've
			# already seen and deleted.
			rtv_utils.ensure_dir_exists( old_dir )

			rtvinfo_fullpath =  os.path.join( config.converted_progs_dir,
				noslash_to_real[rtvinfo_fn] )

			#print "mv %s -> %s" % ( rtvinfo_fullpath, old_dir )
			shutil.move( rtvinfo_fullpath, old_dir )

		del_path = os.path.join( deleted_dir, fn )
		#print "rm %s" % del_path
		os.remove( del_path )


