#!/usr/bin/python

import re, os
import rtv_programmeinfo, rtv_utils, rtv_favourite

fn_re = re.compile(
	"^(\\S*)-(\\d{4}-\\d{2}-\\d{2}_\\d{2}_\\d{2})-(\S*)$" )

FILENAME_DATETIME_FORMAT = "%Y-%m-%d_%H_%M"

class Selection( object ):

	def __init__( self ):
		self.title = None
		self.channel = None
		self.startTime = None
		self.deleteAfterDays = None
		self.priority = 100

	def load( self, filename ):
		m = fn_re.match( filename )
		if not m:
			print( "Selection not recognised - filename '%s'" % filename
				+ "is not of the recognised format." )
		else:
			self.title = m.group( 1 )
			self.channel = m.group( 3 )
			self.startTime = rtv_utils.parse_datetime( m.group( 2 ),
				FILENAME_DATETIME_FORMAT )

	def matches( self, pi ):

		return ( self.title == rtv_utils.prepare_filename( pi.title ) and
				 self.channel == pi.channel and
				 self.startTime == pi.startTime )



def read_favs_and_selections( config, record_only = False ):
	ans = []

	rtv_utils.ensure_dir_exists( config.favourites_dir )

	for fn in os.listdir( config.favourites_dir ):

		if fn[-7:] == ".rtvfav":
			full_fn = os.path.join( config.favourites_dir, fn )

			fav = rtv_favourite.Favourite()
			fav.load( full_fn )

			if ( not record_only ) or ( fav.record == "yes" ):
				ans.append( fav )

	rtv_utils.ensure_dir_exists( config.selections_dir )

	for fn in os.listdir( config.selections_dir ):

		if fn[-7:] == ".rtvsel":
			# Note: no need for full path, as the filename itself
			# contains all the information we need, so we don't need
			# to read the file at all.

			sel = Selection()
			sel.load( fn[:-7] )

			ans.append( sel )

	return ans

