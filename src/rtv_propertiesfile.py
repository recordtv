#!/usr/bin/python

import os

from rtv_abstractpropertiesfile import AbstractPropertiesFile

class PropertiesFile( AbstractPropertiesFile ):

	def set_value( self, key, value ):
		self.__dict__[key] = value

	def get_value( self, key ):
		ret = None
		if key in self.__dict__:
			ret = self.__dict__[key]
		return ret

	def get_keys( self ):
		return self.__dict__.keys()

def test():
	# Testing code:

	pf = PropertiesFile()
	pf.mystr = "Hello2"
	pf.mystrpipe = "|Hello"
	pf.mytuple = ( "x", "y", "z" )
	pf.mylist = [ "xa", "yb", "zc" ]
	pf.myint = 45
	pf.myfloat = 5.8
	pf.mybool = False
	pf.mystrbool = "False"

	filename = "tmp.rtvtestpropertiesfile"

	pf.save( filename )

	pf2 = PropertiesFile()
	pf2.load( filename )

	os.unlink( filename )

	# Convert pf's tuple value to a list before comparison -
	# we convert every sequence type to a list
	pf.mytuple = list( pf.mytuple )

	assert( pf.__dict__ == pf2.__dict__ )

	# TODO: test reading a file with spaces around the =, etc.


