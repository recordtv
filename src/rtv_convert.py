#!/usr/bin/python

import os
import os.path
import re
import shutil
import sys
import rtv_utils, rtv_programmeinfo

allowed_chars_re = re.compile( "[a-zA-z0-9_-]" )

known_filetypes = ( ".ts", ".audiots" )

def _sanitise_filename( fn, max_len ):
	fn = str( fn )
	san_fn = ""
	for ch in fn:
		if allowed_chars_re.match( ch ):
			san_fn += ch
		else:
			san_fn += "_"
	return san_fn[:max_len]

def _get_flv_file_path_from_proginfo( proginfo ):

	startTime = proginfo.startTime.strftime( "%Y-%m-%d_%H_%M" )

	if proginfo.sub_title:
		unsan_file = startTime[:10] + "_" + str( proginfo.sub_title )
	elif proginfo.channel_pretty:
		unsan_file = startTime + "_" + str( proginfo.channel_pretty )
	else:
		unsan_file = startTime + "_" + str( proginfo.channel )

	progname_dir = _sanitise_filename( proginfo.title, 40 )

	if proginfo.destination and proginfo.destination != "":
		dest = os.path.join(
			*( _sanitise_filename( fn, 40 ) for fn in
				proginfo.destination.split( "/" ) )
		)
		ret_dir = os.path.join( dest, progname_dir )
	else:
		ret_dir = progname_dir

	ans = ( ret_dir,
		os.path.join( ret_dir, _sanitise_filename( unsan_file, 40 ) ) )

	return ans


def _get_flv_file_path( rtvinfo, fallback_filename ):
	if not os.path.isfile( rtvinfo ):
		print "** Error: rtvinfo file '%s' not found. **" % rtvinfo
		sys.stdout.flush()
		return ( "", fallback_filename )

	proginfo = rtv_programmeinfo.ProgrammeInfo()
	proginfo.load( rtvinfo )

	return _get_flv_file_path_from_proginfo( proginfo )

def _delete_empty_dirs( config ):
	print
	print "Removing empty directories:"

	for ( dirpath, dirnames, filenames ) in os.walk(
			config.converted_progs_dir ):
		if len( filenames ) == 0 and len( dirnames ) == 0:

			# Don't delete the "deleted" dir
			if ( dirpath.endswith( "deleted" ) ):
				continue

			empty_dir_name = os.path.join(
				config.converted_progs_dir, dirpath )
			print empty_dir_name
			os.rmdir( empty_dir_name )


def convert( config ):

	print
	print "Converting:"

	for fn in os.listdir( config.recorded_progs_dir ):

		last_dot_loc = fn.rfind( "." )
		fn_extn = fn[ last_dot_loc : ]
		fn_stem = fn[ : last_dot_loc ]

		if fn_extn not in known_filetypes:
			continue

		fn = os.path.join( config.recorded_progs_dir, fn )

		rtvinfo = os.path.join( config.recorded_progs_dir,
			fn_stem + ".rtvinfo" )

		(flv_dir, flv_stem) = _get_flv_file_path( rtvinfo, fn_stem )

		if fn_extn == ".audiots":
			output_dir = config.converted_audio_dir
			final_extn = ".ogg"
			convert_command = config.audio_convert_command
		else:
			output_dir = config.converted_progs_dir
			final_extn = ".mp4"
			convert_command = config.convert_command

		rtv_utils.ensure_dir_exists(
			os.path.join( output_dir, flv_dir ) )

		flv_before = os.path.join( output_dir,
			flv_stem + "_converting" + final_extn )

		flv_after = os.path.join( output_dir, flv_stem + final_extn )

		rtvinfo_new = flv_stem + ".rtvinfo"

		old_dir = os.path.join( config.recorded_progs_dir, "old" )

		sys.stdout.flush()
		retval = os.system( convert_command % (
			fn, flv_before, flv_after, old_dir ) )

		if retval != 0:
			print "** Conversion of '%s' returned an error. **" % fn
			continue

		if os.path.isfile( rtvinfo ):
			shutil.move( rtvinfo,
				os.path.join( output_dir, rtvinfo_new ) )

	_delete_empty_dirs( config )


def rename( config ):
	# TODO: only renames video, not audio

	for ( dirpath, dirnames, filenames ) in os.walk(
			config.converted_progs_dir ):

		for fn in filenames:
			fn_extension = fn[-4:]

			if fn_extension not in ( ".flv", ".avi", ".mp4" ):
				continue

			fn_stem = os.path.join( dirpath, fn[ : fn.rfind( "." ) ] )

			rtvinfo_orig = os.path.join( config.converted_progs_dir,
				fn_stem + ".rtvinfo" )

			flv_orig = os.path.join( config.converted_progs_dir,
				fn_stem + fn_extension )

			(flv_dir, flv_stem) = _get_flv_file_path( rtvinfo_orig, fn_stem )

			rtv_utils.ensure_dir_exists(
				os.path.join( config.converted_progs_dir, flv_dir ) )

			flv_after = os.path.join( config.converted_progs_dir,
				flv_stem + fn_extension )

			rtvinfo_after = os.path.join( config.converted_progs_dir,
				flv_stem + ".rtvinfo" )

			if flv_orig != flv_after:
				print "Renaming %s -> %s" % ( flv_orig, flv_after )
				shutil.move( flv_orig, flv_after )

			if rtvinfo_orig != rtvinfo_after:
				print "Renaming %s -> %s" % ( rtvinfo_orig, rtvinfo_after )
				shutil.move( rtvinfo_orig, rtvinfo_after )

	_delete_empty_dirs( config )

# ------ Test code -----

class FakeProgInfo:
	pass

def test_path_from_title_datetime_and_channel():
	proginfo = FakeProgInfo()
	from rtv_saxhandler import SaxHandler
	saxhandler = SaxHandler( None )
	xmltv_time = "20091009094500 +0100"
	proginfo.startTime = saxhandler.parse_time( xmltv_time )
	proginfo.sub_title = None
	proginfo.channel_pretty = "My Channel"
	proginfo.title = "My Programme"
	proginfo.destination = None

	(flv_dir, flv_stem) = _get_flv_file_path_from_proginfo( proginfo )

	assert( flv_dir == "My_Programme" )
	assert( flv_stem == "My_Programme/2009-10-09_09_45_My_Channel" )

def test_path_from_destination_title_datetime_and_channel():
	proginfo = FakeProgInfo()
	from rtv_saxhandler import SaxHandler
	saxhandler = SaxHandler( None )
	xmltv_time = "20091009094500 +0100"
	proginfo.startTime = saxhandler.parse_time( xmltv_time )
	proginfo.sub_title = None
	proginfo.channel_pretty = "My Channel"
	proginfo.title = "My Programme"
	proginfo.destination = "grownups"

	(flv_dir, flv_stem) = _get_flv_file_path_from_proginfo( proginfo )

	assert( flv_dir == "grownups/My_Programme" )
	assert( flv_stem == "grownups/My_Programme/2009-10-09_09_45_My_Channel" )


def test_multi_destination():
	proginfo = FakeProgInfo()
	from rtv_saxhandler import SaxHandler
	saxhandler = SaxHandler( None )
	xmltv_time = "20091009094500 +0100"
	proginfo.startTime = saxhandler.parse_time( xmltv_time )
	proginfo.sub_title = None
	proginfo.channel_pretty = "My Channel"
	proginfo.title = "My Programme"
	proginfo.destination = "grownups/movies"

	(flv_dir, flv_stem) = _get_flv_file_path_from_proginfo( proginfo )

	assert( flv_dir == "grownups/movies/My_Programme" )
	assert(
		flv_stem == "grownups/movies/My_Programme/2009-10-09_09_45_My_Channel" )


def test( config ):
	test_path_from_title_datetime_and_channel()
	test_path_from_destination_title_datetime_and_channel()
	test_multi_destination()


