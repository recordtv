#!/usr/bin/python

import os
import os.path
import shutil

import rtv_schedule

original_old_dir = "test/files/norerecordold"
old_dir          = "test/files/norerecordold-copy"
converted_dir    = "test/files/norerecordconv"

def get_dir_tree( dr ):
	ret = {}
	for (dirpath, dirnames, filenames) in os.walk( dr ):
		ret[dirpath] = filenames
	return ret

def test_slow_find_old_programmes_map():

	if os.path.isdir( old_dir ):
		shutil.rmtree( old_dir )

	shutil.copytree( original_old_dir, old_dir )

	sch = rtv_schedule.Schedule( None )

	old_converted_tree = get_dir_tree( converted_dir )

	mp = sch.find_old_programmes_map( old_dir, converted_dir )

	# We found the right titles and sub-titles
	assert( mp == {
		"Pocoyo"             : { "Pato's Paintings" : 1,
								 "Monster Mystery"  : 1, },
		"The West Wing"      : { "In the Room"      : 1, },
		"Thomas and Friends" : { "A Smooth Ride"    : 1, },
		} )

	# We deleted the rtvinfos that didn't contain sub-titles
	# or did contain unique_subtitles=False
	dr_list = os.listdir( old_dir )
	dr_list.sort()

	assert( dr_list == [
		"2008-10-10_Pato_s_Paintings.rtvinfo",
		"2008-10-13_Monster_Mystery.rtvinfo",
		"2008-11-16_In_the_Room.rtvinfo",
		] )

	new_converted_tree = get_dir_tree( converted_dir )

	assert( old_converted_tree == new_converted_tree )

	shutil.rmtree( old_dir )


def test_slow():
	test_slow_find_old_programmes_map()

