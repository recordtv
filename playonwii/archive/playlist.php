<?php

include( "paths.php" );

?>


<rss version="2.0" xmlns:media="http://search.yahoo.com/mrss">
	<channel>
		<title><?php print $playlist_title; ?></title>

<?php

$filenames = array();
$handle = opendir( $video_path );
if( $handle )
{
    while( ( $filename = readdir( $handle ) ) )
    {
        $fl = strlen( $filename );
        if( $fl > 4 and substr( $filename, $fl - 4 ) == ".flv"
            or substr( $filename, $fl - 4 ) == ".avi" )
        {
            $filenames[] = $filename;
        }
    }
}

$num = 0;
sort( $filenames );

foreach( $filenames as $filename )
{
    $file_title = $filename;
    $file_uri = $video_uri . "/" . $filename;
?>
        
		<item>
			<title><?php print $file_title; ?></title>
			<enclosure url="<?php print $file_uri; ?>" type="video/x-flv" />

		</item>

<?php
}

?>
        
	</channel>
</rss>

