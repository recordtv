<?php

include_once "functions.php";

$file_path = "$videos_uri/" . prepare_filename( $_GET['filename'] );

$dotpos = strrpos( $file_path, '.' );
$extension = substr( $file_path, $dotpos + 1 );

?>

<html>

<head>
    <title>Play file</title>
</head>

<body>

<video controls>
  <source
    src="<?php print "$file_path" ?>"
    type="video/<?php print "$extension" ?>"
    />

  Your browser does not support the <code>video</code> element.
Download the <a href="<?php print "$file_path" ?>">Video File</a>.
</video>

<p><a href="<?php print "$file_path" ?>">Video File</a>.</p>

</body>

</html>

