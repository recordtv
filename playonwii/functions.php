<?php

$videos_dir = "/home/andy/Videos/Wii";
$deleted_dir = "$videos_dir/deleted";
$videos_uri = "/filesvideo";

$allowed_chars_re = "/[\\/0-9a-zA-Z_-]/";

# Compare with rtv_utils.py's prepare_filename method
function prepare_filename( $filename )
{
    global $allowed_chars_re;
    
    $len = strlen( $filename );
    $dotpos = strrpos( $filename, '.' );
    $extension = substr( $filename, $dotpos );
    $filename = substr( $filename, 0, $dotpos );

    if ( $extension == ".mp4" )
    {
        $dotpos = strrpos( $filename, '.' );
        $tmp = substr( $filename, $dotpos );
        if ( $tmp == ".avi_high" )
        {
            $extension = $tmp . $extension;
            $filename = substr( $filename, 0, $dotpos );
        }
    }

    if ( $extension != ".flv" and
         $extension != ".avi"  and
         $extension != ".mp4"  and
         $extension != ".avi_high.mp4"  and
         $extension != ".webm" )
    {
        return "not_an_flv";
    }

    $ans = sanitise_filename( $filename );

    return $ans . $extension;
}

function sanitise_filename( $filename )
{
    global $allowed_chars_re;

    $len = strlen( $filename );

    $ans = "";

    for( $i = 0; $i < $len; $i++ )
    {
        $char = $filename[$i];
        if( preg_match( $allowed_chars_re, $char ) )
        {
            $ans .= $char;
        }
        else
        {
            $ans .= "_";
        }
    }

    return $ans;
}

function ends_with( $haystack, $needle )
{
    return (
        substr_compare(
            $haystack,
            $needle,
            strlen( $haystack ) - strlen( $needle )
        )
        == 0
    );
}

?>

