<?php
ob_start();
include_once "functions.php";

$delete_file_name = prepare_filename( $_GET['filename'] );

$delete_file_name = str_replace( "/", "_", $delete_file_name );

$deleted_file_path = "$deleted_dir/" . $delete_file_name;

$success = touch( $deleted_file_path );

if( !$success )
{
    header("HTTP/1.1 500 Internal Server Error");
    print "Unable to delete file.";
}
else
{
    print "File deleted.";
}

?>
