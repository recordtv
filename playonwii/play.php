<?php

include_once "functions.php";

$file_path = "$videos_uri/" . prepare_filename( $_GET['filename'] );

$width = 566;
$height = 330;

?>

<html>

<head>
    <title>Play file</title>
    <script type="text/javascript" src="swfobject.js"></script>
</head>

<body style="background-color: black; color: white">

<p id='player1'>No flash player!</p>
<script type='text/javascript'>
    var s1 = new SWFObject('flvplayer.swf','single','566','330','7');
    s1.addParam('allowfullscreen','false');
    s1.addVariable('file','<?php print "$file_path" ?>');
    s1.addVariable('showvolume','false');
    s1.addVariable('autostart','true');
    s1.addVariable('displayheight','330');
    s1.addVariable('displaywidth','566');
    s1.addVariable('largecontrols','true');
    s1.addVariable('overstretch','true');
    s1.addVariable('usefullscreen','false');
    s1.addVariable('streamscript','lighttpd');
    s1.write('player1');
</script>
</p>

</body>

</html>

