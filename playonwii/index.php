<?php

include_once( "functions.php" );

$extra_path = "";
if ( array_key_exists( "path", $_GET ) )
{
    $extra_path = sanitise_filename( $_GET["path"] );
}

$excluded_preprepare = array();
if ( array_key_exists( "exclude", $_GET ) )
{
    $excluded_preprepare = explode( ":", $_GET["exclude"] );
}

$excluded = array();

foreach( $excluded_preprepare as $ex )
{
    $excluded[] = sanitise_filename( $ex );
}

$full_videos_dir = $videos_dir;

if( $extra_path )
{
    $full_videos_dir .= "/$extra_path";
    if( !ends_with( $extra_path, "/" ) )
    {
        $extra_path .= "/";
    }
}
else
{
    $extra_path = "";
}

function is_video_dir( $full_path )
{
    if( substr_compare( $full_path, "/.", -2 ) == 0 or
        substr_compare( $full_path, "/..", -3 ) == 0 or
        substr_compare( $full_path, "/deleted", -8 ) == 0 or
        substr_compare( $full_path, "/tvguide", -8 ) == 0 )
    {
        return false;
    }

    return is_dir( $full_path );
}

function is_not_excluded( $filename )
{
    global $excluded;

    return !in_array( $filename, $excluded );
}

function get_filenames_recursive( &$filenames, $top_dir, $sub_dir = "" )
{
    $handle = opendir( "$top_dir/$sub_dir" );
    if( $handle )
    {
        while( ( $filename = readdir( $handle ) ) )
        {
            if( strlen( $sub_dir ) > 0 )
            {
                $filepath = "$sub_dir/$filename";
            }
            else
            {
                $filepath = $filename;
            }
            if( is_not_excluded( $filename ) &&
                is_video_dir( "$top_dir/$sub_dir/$filename" ) )
            {
                get_filenames_recursive( $filenames, $top_dir, $filepath );
            }
            else
            {
                $filenames[$filepath] = '';
            }
        }
        closedir($handle);
    }
}

function pretty_date( $date )
{
    global $num_to_month;

    $hour   = substr( $date, 11, 2 );
    $minute = substr( $date, 14, 2 );
    //$second omitted
    $month  = substr( $date,  5, 2 );
    $day    = substr( $date,  8, 2 ) ;
    $year   = substr( $date,  0, 4 );

    if(
        is_numeric( $hour   ) &&
        is_numeric( $minute ) &&
        is_numeric( $month  ) &&
        is_numeric( $day    ) &&
        is_numeric( $year   )
    )
    {
        $tm = mktime(
            (int)( $hour ),
            (int)( $minute ),
            0,
            (int)( $month ),
            (int)( $day ),
            (int)( $year )
        );

        return date( "D d M, H:i", $tm );
    }
    else
    {
        return "";
    }
}

function single_quote_attribute_escape( $value )
{
    return str_replace( "'", "&apos;", $value );
}

function get_info_from_file( $fn )
{
    $title = "";
    $sub_title = "";
    $description = "";
    $date = "";
    $channel = "";

    $handle = fopen( $fn, "r" );

    if( $handle )
    {
        while ( !feof( $handle ) )
        {
            $line = fgets( $handle );
            $line = substr( $line, 0, -1 );

            $splitline = split( "=", $line, 2 );
            if( count( $splitline ) < 2 )
            {
                continue;
            }

            list( $k, $v ) = $splitline;
            switch( $k )
            {
                case "title":
                {
                    $title = $v;
                    break;
                }
                case "sub_title":
                {
                    $sub_title = $v;
                    break;
                }
                case "description":
                {
                    $description = $v;
                    break;
                }
                case "startTime":
                {
                    $date = $v;
                    break;
                }
                case "channel_pretty":
                {
                    $channel = $v;
                    break;
                }
            }
        }

        fclose( $handle );
    }

    return array( $title, $date, $channel, $sub_title, $description );
}

function get_info_from_filename( $fn )
{
    if( preg_match( '/(.*\\/)?([^\\/]*)\\/(.*)-.*$/', $fn, $matches ) )
    {
        $title = $matches[2];
        $date = $matches[3];
    }
    else if( preg_match( '/(.*\\/)?([^\\/]*)\\/.*$/', $fn, $matches ) )
    {
        $title = $matches[2];
        $date = "";
    }
    else
    {
        $title = substr( $fn, 0, strlen( $fn ) - 4 );
        $date = "";
    }

    if( preg_match( '/.*\\/(.*)\\..*$/', $fn, $matches ) )
    {
        $sub_title = $matches[1];
    }
    else
    {
        $sub_title = $fn;
    }

    return array( $title, $date, $sub_title );
}

function get_info( $filename, $filenames )
{
    global $full_videos_dir;

    $title = Null;
    $date = Null;
    $channel = Null;
    $sub_title = Null;
    $description = Null;

    if( preg_match( '/^(.*?)\\.(flv|avi|avi_high\\.mp4|webm|mp4)$/', $filename, $matches ) )
    {

        $infofn = $matches[1] . ".rtvinfo";

        if( array_key_exists( $infofn, $filenames ) )
        {
            list( $title, $date, $channel, $sub_title, $description ) =
                get_info_from_file( $full_videos_dir . "/" . $infofn );
        }
        else
        {
            list( $title, $date, $sub_title ) =
                get_info_from_filename( $filename );

            $channel = "";
            $description = "";
        }
    }

    return array( $title, $date, $channel, $sub_title, $description );
}

// This is a hash filename -> nothing of all files in the videos directory
// and subdirectories
$filenames = array();
get_filenames_recursive( $filenames, $full_videos_dir );

// This is a hash filename -> nothing of all files in the deleted directory
$deleted_filenames = array();
$handle = opendir( $deleted_dir );
if( $handle )
{
    while( ( $filename = readdir( $handle ) ) )
    {
        $deleted_filenames[$filename] = '';
    }

    closedir($handle);
}


// This is a hash title->array( array( filenumber, date, filename, channel ) )
$titles = array();
$num = 0;

$nondeleted_filenames = array();

foreach ( $filenames as $fn => $blank )
{
    $modified_fn = str_replace( "/", "_", "$extra_path$fn" );
    if( !array_key_exists( $modified_fn, $deleted_filenames ) )
    {
        $nondeleted_filenames[$fn] = '';
    }
}
#array_diff_key( $filenames, $deleted_filenames );

$sorted_fns = array_keys( $nondeleted_filenames );
sort( $sorted_fns );

foreach( $sorted_fns as $filename )
{
    list( $title, $date, $channel, $sub_title, $description ) = get_info(
        $filename, $nondeleted_filenames );

    if( $title != Null )
    {
        $titles[$title][] = array( $num, $date, $channel, $sub_title,
            $description, $filename );
        $num++;
    }
}

?>

<html>

<head>
<title>Recorded programmes</title>
<style type="text/css">
    body {
        font-family: verdana, sans-serif;
        text-align: center;
    }
    a {
        text-decoration: none;
        color: black;
    }
    a:hover {
        color: red;
    }
    a.deletelink {
        color:red;
        font-size: xx-small;
    }
    a.title {
        color: blue;
    }
    span.smalltime {
        font-size: smaller;
        color: gray;
    }
    span.smalltime:hover {
        color: red;
    }
    h2 {
        font-size: x-large;
        font-weight: normal;
        color: blue;
        margin: 2px;
    }
    td.dates {
        font-size: small;
    }
</style>
<script language="JavaScript">

function makeRequest( url, arg )
{
    var httpRequest;

    if( window.XMLHttpRequest )  // Mozilla, Safari etc.
    {
        httpRequest = new XMLHttpRequest();
    }
    else if( window.ActiveXObject )  // IE
    {
        try
        {
            httpRequest = new ActiveXObject( "Msxml2.XMLHTTP" );
        }
        catch( e )
        {
            try
            {
                httpRequest = new ActiveXObject( "Microsoft.XMLHTTP" );
            }
            catch( e )
            {
            }
        }
    }

    if( !httpRequest )
    {
        return false;
    }

    httpRequest.onreadystatechange = function()
    {
        receiveAnswer( httpRequest, arg );
    };

    httpRequest.open('GET', url, true);
    httpRequest.send('');
}

function receiveAnswer( httpRequest, prog_filename )
{
    if( httpRequest.readyState == 4 )
    {
        if( httpRequest.status != 200 )
        {
            document.location = "delete_error.php?filename=" + prog_filename
        }
    }
}


function mouse_over( tr_id )
{
    tr_el = document.getElementById( tr_id );
    tr_el.style.backgroundColor = '#ffaaaa';
}

function mouse_out( tr_id )
{
    tr_el = document.getElementById( tr_id );
    tr_el.style.backgroundColor = 'transparent';
}

function title_click( table_id )
{
    table_el = document.getElementById( table_id );
    if( table_el.style.display == 'inline' )
    {
        table_el.style.display = 'none';
    }
    else
    {
        table_el.style.display = 'inline';
    }
}


function delete_prog( prog_filename )
{
    makeRequest( 'delete.php?filename=' + prog_filename,
        prog_filename );
    tr_el = document.getElementById( 'tr_' + prog_filename );
    tr_el.style.display = 'none';
}

</script>
</head>

<body>
<h1>Recorded programmes</h1>

<center>
<?php
        ksort( $titles );
        $table_counter = 0;
        foreach( $titles as $title => $arr )
        {
            print "<h2><a class='title' href='javascript: title_click( \"table_$table_counter\" )'>$title</a></h2>\n";

            print "<table id='table_$table_counter' style='display: none' width='90%' cellpadding='0' cellspacing='0' border='0'>";
            foreach( $arr as $lst )
            {
                list( $num, $date, $channel, $sub_title, $description,
                    $filename ) = $lst;

                $dotpos = strrpos( $filename, '.' );
                $ext = substr( $filename, $dotpos );
                $play_url = "$videos_uri/$extra_path$filename";
                if( $ext == ".webm" || $ext == ".mp4" )
                {
                    $play_url = "play-html5.php?filename=$extra_path$filename";
                }
                else if( $ext == ".flv" )
                {
                    $play_url = "play.php?filename=$extra_path$filename";
                }

                print "<tr id='tr_$extra_path$filename'><td><a href='$play_url' style='padding-right: 10px'";

                print " title='".single_quote_attribute_escape( $description )
                    ."'";

                print ">";

                if( $sub_title )
                {
                    print $sub_title . " <span class='smalltime'>";
                }

                $dtchan = pretty_date( $date );

                if( $channel )
                {
                    $dtchan .= " on $channel";
                }

                if( $dtchan )
                {
                    if( $sub_title )
                    {
                        print "(";
                    }

                    print $dtchan;

                    if( $sub_title )
                    {
                        print ")";
                    }
                }

                if( $sub_title )
                {
                    print "</span>";
                }

                print "</a></td>";
                print "<td><a class='deletelink' onmouseover='mouse_over(\"tr_$extra_path$filename\")' onmouseout='mouse_out(\"tr_$extra_path$filename\")' href='javascript: delete_prog( \"$extra_path$filename\" )'>[DELETE]</a></td></tr>\n";
            }
            print "</table>\n";
            $table_counter += 1;
        }
?>
</center>

</body>

</html>

