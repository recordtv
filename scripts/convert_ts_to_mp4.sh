#!/bin/bash

# Usage:
#
# ./convert_ts_to_mp4.sh a/file.ts b/c/prog_converting.mp4 b/c/prog.mp4 a/old
#
# Converts the supplied MPEG-2 ts file to MPEG-4 format.
#

TS_FILENAME="$1"
CONV_FILENAME="$2"
FINAL_FILENAME="$3"
OLD_DIR="$4"


#BITRATE="2500k"
BITRATE="2500"
SIZE=

if [[ "$CONV_FILENAME" == **/kids/** ]]; then
    BITRATE="1000"
    #BITRATE="1000k"
    #SIZE="-s:v 640x480"
    SIZE=",scale=640:480"
    #SIZE="-w 640 -l 480"
fi

function thumbnail()
{
    INPUT="$1"
    START="$2"
    NUM="$3"
    SIZE=240x176

    avconv -loglevel quiet -ss "$START" -i "$INPUT" -s "$SIZE" -vframes 1 "$INPUT-$NUM.png"
}

if [ ! -f "$FINAL_FILENAME" ]; then
{
    echo $TS_FILENAME

    avconv -i "$TS_FILENAME" -acodec copy -b:v $BITRATE -c:v mpeg4 $SIZE "$CONV_FILENAME" > avconv_out.txt 2>&1
    #mencoder -quiet "$TS_FILENAME" -o "$CONV_FILENAME" -vf pp=li$SIZE -oac mp3lame -ovc lavc -lavcopts vcodec=mpeg4:vbitrate=$BITRATE > avconv_out.txt 2>&1
    #HandBrakeCLI -i "$TS_FILENAME" -o "$CONV_FILENAME" -vb $BITRATE $SIZE > avconv_out.txt 2>&1

    RETVAL=$?

    mkdir -p "$OLD_DIR"
    mv "$TS_FILENAME" "$OLD_DIR"

    if [[ $RETVAL != 0 ]]; then
    {
        echo '$ avconv -i "$TS_FILENAME" -acodec copy -b:v $BITRATE -c:v mpeg4 $SIZE "$CONV_FILENAME"'
        #echo 'mencoder -quiet "$TS_FILENAME" -o "$CONV_FILENAME" -vf pp=li,scale=$SIZE -oac mp3lame -ovc lavc -lavcopts vcodec=mpeg2video:vbitrate=$BITRATE'
        #echo 'HandBrakeCLI -i "$TS_FILENAME" -o "$CONV_FILENAME" -vb $BITRATE $SIZE'

        cat avconv_out.txt

        echo "** Error: avconv returned '"$RETVAL"' **"

        echo "echo Conversion of "$TS_FILENAME" failed." | at now

        exit 2
    }; fi

    mv "$CONV_FILENAME" "$FINAL_FILENAME"

    rm avconv_out.txt

    thumbnail "$FINAL_FILENAME"  300 1
    thumbnail "$FINAL_FILENAME"  600 2
    thumbnail "$FINAL_FILENAME" 1200 3
}
else
{
    echo "** Error: output filename already exists: " $FINAL_FILENAME " **"
    exit 5
}; fi



