#!/bin/bash

# Usage:
#
# ./convert_ts_to_mp2.sh a/file.ts b/c/prog_converting.avi b/c/prog.avi a/old
#
# Converts the supplied MPEG-2 ts file to MPEG-2 640x480 format.
#

TS_FILENAME="$1"
CONV_FILENAME="$2"
MP2_FILENAME="$3"
OLD_DIR="$4"

SIZE=640x480

if [ ! -f "$MP2_FILENAME" ]; then
{
    #echo "Converting $TS_FILENAME to MPEG-2 .avi"
    echo $TS_FILENAME

    #echo '$ mencoder -quiet '"'$TS_FILENAME'"' -o '"'$CONV_FILENAME'"' -vf pp=li,scale=640:480 -oac mp3lame -ovc lavc -lavcopts vcodec=mpeg2video:vbitrate=1200'

    mencoder -quiet "$TS_FILENAME" -o "$CONV_FILENAME" -vf pp=li,scale=640:480 -oac mp3lame -ovc lavc -lavcopts vcodec=mpeg2video:vbitrate=1200 > mencoder_out.txt 2>&1
    
    RETVAL=$?

    mkdir -p "$OLD_DIR"
    mv "$TS_FILENAME" "$OLD_DIR"

    if [[ $RETVAL != 0 ]]; then
    {
        echo '$ mencoder -quiet '"'$TS_FILENAME'"' -o '"'$CONV_FILENAME'"' -vf pp=li,scale=640:480 -oac mp3lame -ovc lavc -lavcopts vcodec=mpeg2video:vbitrate=1200'

        cat mencoder_out.txt

        echo "** Error: mencoder returned '"$RETVAL"' **"

	echo "echo Conversion of "$TS_FILENAME" failed." | at now

        exit 2
    }; fi

    mv "$CONV_FILENAME" "$MP2_FILENAME"

    rm mencoder_out.txt
}
else
{
    echo "** Error: .mp2 filename already exists: " $MP2_FILENAME " **"
    exit 5
}; fi



