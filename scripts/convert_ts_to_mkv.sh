#!/bin/bash

# Usage:
#
# ./convert_ts_to_mkv.sh a/file.ts b/c/prog_converting.mkv b/c/prog.mkv a/old
#
# Converts the supplied MPEG-2 ts file to h264/aac mkv (suitable for iPad)
#

TS_FILENAME="$1"
CONV_FILENAME="$2"
FINAL_FILENAME="$3"
OLD_DIR="$4"

if [ ! -f "$FINAL_FILENAME" ]; then
{
    echo $TS_FILENAME

    avconv -i "$TS_FILENAME" -c:a libfaac -c:v libx264 "$FINAL_FILENAME" > avconv_out.txt 2>&1

    RETVAL=$?

    mkdir -p "$OLD_DIR"
    mv "$TS_FILENAME" "$OLD_DIR"

    if [[ $RETVAL != 0 ]]; then
    {
        echo 'avconv -i '"$TS_FILENAME"' -c:a libfaac -c:v libx264 '"$FINAL_FILENAME"

        cat avconv_out.txt

        echo "** Error: avconv returned '"$RETVAL"' **"

	echo "echo Conversion of "$TS_FILENAME" failed." | at now

        exit 2
    }; fi

    mv "$CONV_FILENAME" "$FINAL_FILENAME"

    rm avconv_out.txt
}
else
{
    echo "** Error: output filename already exists: " $FINAL_FILENAME " **"
    exit 5
}; fi



