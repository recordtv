#!/bin/bash

# Usage:
#
# ./convert_audiots_to_ogg.sh file.ts file_converting.ogg prog.ogg old
#
# Converts the supplied MPEG-2 audio ts file to an Ogg Vorbis file
#

TS_FILENAME="$1"
CONV_FILENAME="$2"
OGG_FILENAME="$3"
OLD_DIR="$4"

if [ ! -f "$OGG_FILENAME" ]; then
{
    echo "Converting $TS_FILENAME to Ogg Vorbis"

    echo '$ ffmpeg -i "'$TS_FILENAME'" -acodec vorbis -aq 60 "'$CONV_FILENAME'"'

    ffmpeg -i "$TS_FILENAME" -acodec vorbis -aq 60 "$CONV_FILENAME" > ffmpeg_out.txt 2>&1

    RETVAL=$?

    mkdir -p "$OLD_DIR"
    mv "$TS_FILENAME" "$OLD_DIR"

    if [[ $RETVAL != 0 ]]; then
    {
        #echo '$ ffmpeg -i "'$TS_FILENAME'" -acodec vorbis -aq 60 "'$CONV_FILENAME'"'

        cat mencoder_out.txt

        echo "Error: ffmpeg returned '"$RETVAL"'"

        exit 2
    }; fi

    mv "$CONV_FILENAME" "$OGG_FILENAME"

    rm ffmpeg_out.txt
}
else
{
    echo "Error: .ogg filename already exists: " $OGG_FILENAME
    exit 5
}; fi



