#!/bin/bash

function thumbnail()
{
    INPUT="$1"
    START="$2"
    NUM="$3"
    SIZE=240x176

    avconv -loglevel quiet -ss "$START" -i "$INPUT" -s "$SIZE" -vframes 1 "$INPUT-$NUM.png"
}

thumbnail "$1" 300 1
thumbnail "$1" 600 2
thumbnail "$1" 1200 3

