#!/bin/bash

# Usage:
#
# ./convert_ts_to_flv.sh a/file.ts b/c/conv_unproc.flv b/c/conv.flv a/old
#
# Converts the supplied MPEG-2 ts file to FLV format.
#

TS_FILENAME="$1"
FLV_UP_FILENAME="$2"
FLV_FILENAME="$3"
OLD_DIR="$4"

SIZE=566x330

UP_SUFFIX=_before_flvtool2

if [ ! -f "$FLV_UP_FILENAME" -a \
     ! -f "$FLV_FILENAME" ]; then
{
    echo "Converting $TS_FILENAME"
    ffmpeg -re -r 25 -y -i "$TS_FILENAME" -b 900k -async 1000 -ar 44100 -s $SIZE -f flv -ar 22050 -acodec mp3 "$FLV_UP_FILENAME" 2> ffmpeg_stderr.txt

    RETVAL=$?

    #echo 'mkdir -p "'$OLD_DIR'"'
    mkdir -p "$OLD_DIR"
    #echo 'mv "'$TS_FILENAME'" "'$OLD_DIR'"'
    mv "$TS_FILENAME" "$OLD_DIR"
    #echo 'mv returned ' $?

    if [[ $RETVAL != 0 ]]; then
    {
        echo '$ ffmpeg -re -r 25 -y -i "'$TS_FILENAME'" -b 900k -async 1000 -ar 44100 -s '$SIZE' -f flv -ar 22050 -acodec mp3 "'$FLV_FILENAME'"'

        cat ffmpeg_stderr.txt

        echo "Error: ffmpeg returned '"$RETVAL"'"

        exit 2
    }; fi

    rm ffmpeg_stderr.txt
}
else
{
    if [ -f "$FLV_FILENAME" ]; then
    {
        echo "Error: .flv filename already exists: " $FLV_FILENAME
        exit 5
    }
    elif [ -f "$FLV_UP_FILENAME" ]; then
    {
        echo "unprocessed filename already exists.  Just converting it: " $FLV_UP_FILENAME

        #echo 'mkdir -p "'$OLD_DIR'"'
        mkdir -p "$OLD_DIR"
        #echo 'mv "'$TS_FILENAME'" "'$OLD_DIR'"'
        mv "$TS_FILENAME" "$OLD_DIR"
        #echo 'mv returned ' $?
    }
    else
    {
        echo "Error: did not convert but neither .flv nor unprocessed files exist."
        exit 6
    }; fi
}; fi


if [ -f $FLV_UP_FILENAME ]; then
{
    if [ ! -f $FLV_FILENAME ]; then
    {
        /home/andy/cvs/flvtoolpp/flvtoolpp "$FLV_UP_FILENAME" "$FLV_FILENAME"
        RETVAL=$?

        if [[ $RETVAL == 0 ]]; then
        {
            rm "$FLV_UP_FILENAME"
        }
        else
        {
            echo "Error: flvtoolpp returned non-zero!"
            exit 3
        }; fi
    }
    else
    {
        echo "Error: $FLV_UP_FILENAME would clash with existing file $FLV_FILENAME!"
        exit 4
    }; fi
}
else
{
    echo "Error: unprocessed file does not exist after ffmpeg."
    exit 7
}; fi


