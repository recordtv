#!/bin/bash

# Example:

# ./record_dvb_to_ts.sh "BBC ONE" eastenders 1800 \
#     "../scheduled_recordings/eastenders.rtvinfo"

# ... will record into a file called eastenders.ts

# An optional fifth argument specifies the file extension.  Default: .avi

CHANNEL="$1"
FILENAME="$2"
LENGTH_SECONDS="$3"
INFO_FILENAME="$4"
FILE_EXTENSION="$5"

if [ "$FILE_EXTENSION" == "" ]; then
{
    FILE_EXTENSION="ts"
}; fi

date

echo '$ killall mplayer'
killall mplayer

echo '$ killall mencoder'
killall mencoder

if [ "$?" == "0" ]; then
{
    echo '$ sleep 10s'
    sleep 10s
}; fi


echo '$ mv "'$INFO_FILENAME'" "'$FILENAME'.rtvinfo"'
mv "$INFO_FILENAME" "$FILENAME.rtvinfo"

date

echo '$ mencoder -o "'$FILENAME'_recording.'$FILE_EXTENSION'" "dvb://'$CHANNEL'" -vf pp=li,scale=640:480 -oac mp3lame -ovc lavc -lavcopts vcodec=mpeg2video:vbitrate=1200 &'

#-cache 256
mencoder -o "${FILENAME}_recording.$FILE_EXTENSION" "dvb://$CHANNEL" -vf pp=li,scale=640:480 -oac mp3lame -ovc lavc -lavcopts vcodec=mpeg2video:vbitrate=1200 &

date

echo '$ sleep '$LENGTH_SECONDS's'
sleep ${LENGTH_SECONDS}s

date

echo '$ kill %mencoder'
kill %mencoder

echo '$ sleep 5s'
sleep 5s

echo '$ mv "'$FILENAME'_recording.'$FILE_EXTENSION'" "'$FILENAME.$FILE_EXTENSION'"'
mv "$FILENAME_recording.$FILE_EXTENSION" "$FILENAME.$FILE_EXTENSION"

