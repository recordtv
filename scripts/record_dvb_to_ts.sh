#!/bin/bash

# Example:

# ./record_dvb_to_ts.sh "BBC ONE" eastenders 1800 \
#     "../scheduled_recordings/eastenders.rtvinfo"

# ... will record into a file called eastenders.ts

# An optional fifth argument specifies the file extension.  Default: .ts

CHANNEL="$1"
FILENAME="$2"
LENGTH_SECONDS="$3"
INFO_FILENAME="$4"
FILE_EXTENSION="$5"

if [ "$FILE_EXTENSION" == "" ]; then
{
    FILE_EXTENSION="ts"
}; fi

date

echo '$ killall mplayer'
killall mplayer

echo '$ killall radio_4'
killall radio_4

#echo '$ killall mencoder'
#killall mencoder

if [ "$?" == "0" ]; then
{
    echo '$ sleep 10s'
    sleep 10s
}; fi


echo '$ mv "'$INFO_FILENAME'" "'$FILENAME'.rtvinfo"'
mv "$INFO_FILENAME" "$FILENAME.rtvinfo"

date

echo '$ mplayer -cache 8192 -dumpstream -dumpfile "'$FILENAME'.'$FILE_EXTENSION'.recording" "dvb://'$CHANNEL'" &'
mplayer -cache 8192 -dumpstream -dumpfile "$FILENAME.$FILE_EXTENSION.recording" "dvb://$CHANNEL" &

date

echo '$ sleep '$LENGTH_SECONDS's'
sleep ${LENGTH_SECONDS}s

date

echo '$ kill %mplayer'
kill %mplayer

echo '$ sleep 5s'
sleep 5s

echo '$ mv "'$FILENAME'.'$FILE_EXTENSION'.recording" "'$FILENAME'.'$FILE_EXTENSION'"'
mv "$FILENAME.$FILE_EXTENSION.recording" "$FILENAME.$FILE_EXTENSION"

LARGEFILE=`ls -s "$FILENAME.$FILE_EXTENSION" | cut -d " " -f 1 | xargs echo "40720 > " | bc`

if [[ "$LARGEFILE" -ne "0" ]]; then
{
	echo "echo recording "$FILENAME"."$FILE_EXTENSION" failed." | at now
}; fi

