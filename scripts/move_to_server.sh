#!/bin/bash

echo $1
ssh 10.0.1.8 mkdir -p "Videos/Wii/$1"
ssh 10.0.1.8 rmdir "Videos/Wii/$1"

if [[ "$1" == *.rtvinfo ]]; then
    echo cp $1 ../Recorded/old/
    cp $1 ../Recorded/old/
fi

scp -q "$1" "10.0.1.8:Videos/Wii/$1" && rm "$1"

